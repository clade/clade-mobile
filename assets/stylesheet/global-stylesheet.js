const globalStyles = {
  color: {
    blue: '#1a1e34',
    pink: '#FD5D93',
    dark_gray: '#2a2a2b',
    light_gray: '#eeeeee',
    stock_green: '#55dfb1',
    stock_red: '#e5004b',
  },
};

export default globalStyles;
