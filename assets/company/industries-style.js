const industriesStyle = [
    {
        "industry_id": 1,
        "industry": "Software & Services",
        "icon": "cpu",
        "color":"#0c56aa"
    },
    {
        "industry_id": 2,
        "industry": "Banks and Finance",
        "icon": "dollar-sign",
        "color":"#138049"
    },
    {
        "industry_id": 3,
        "industry": "Pharmaceuticals & Biotechnology",
        "icon": "feather",
        "color":"#e28030"
    },
    {
        "industry_id": 4,
        "industry": "Retail and Consumables",
        "icon": "shopping-cart",
        "color":"#ab0b4c"
    },
    {
        "industry_id": 5,
        "industry": "Automobiles & Components",
        "icon": "truck",
        "color":"#5a5a5b"
    },
    {
        "industry_id": 6,
        "industry": "Media",
        "icon": "message-square",
        "color":"#d580be"
    },
    {
        "industry_id": 7,
        "industry": "Food, Beverage & Tobacco",
        "icon": "coffee",
        "color":"#ff4d00"
    },
    {
        "industry_id": 8,
        "industry": "Telecoms",
        "icon": "tablet",
        "color":"#ff667b"
    },
    {
        "industry_id": 9,
        "industry": "Real, Commercial & Supplies",
        "icon": "home",
        "color":"#2aa3b9"
    },
    {
        "industry_id": 10,
        "industry": "Household & Personal",
        "icon": "user",
        "color":"#c792ff"
    },
    {
        "industry_id": 11,
        "industry": "Semiconductor Equipment & Products",
        "icon": "box",
        "color":"#ff6b64"
    },
    {
        "industry_id": 12,
        "industry": "Capital Goods",
        "icon": "aperture",
        "color":"#611d79"
    },
    {
        "industry_id": 13,
        "industry": "Hotels, Restaurants & Leisure",
        "icon": "sunset",
        "color":"#e59400"
    },
    {
        "industry_id": 14,
        "industry": "Communications Equipment",
        "icon": "grid",
        "color":"#e5068c"
    },
    {
        "industry_id": 15,
        "industry": "Telecommunication Services",
        "icon": "phone",
        "color":"#e5068c"
    },
    {
        "industry_id": 16,
        "industry": "Utilities, Energy and Refining",
        "icon": "battery-charging",
        "color":"#0688e5"
    },
    {
        "industry_id": 17,
        "industry": "Health Care Equipment & Services",
        "icon": "heart",
        "color":"#8003e5"
    }
];

export default industriesStyle;
