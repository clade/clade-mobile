import React from 'react';
import {StyleSheet} from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';
import globalStyles from '../../assets/stylesheet/global-stylesheet';

function CladeAlert(props: {
  title: string,
  alert: {message: string, status: boolean},
  onConfirmPressed: Function,
}) {
  return (
    <AwesomeAlert
      show={props.alert.status}
      title={props.title}
      titleStyle={style.black_text}
      message={props.alert.message}
      messageStyle={style.black_text}
      showConfirmButton={true}
      confirmText="  OK  "
      closeOnTouchOutside={false}
      onConfirmPressed={props.onConfirmPressed}
      confirmButtonColor={globalStyles.color.clade_pink}
    />
  );
}

const style = StyleSheet.create({
  black_text: {
    color: 'black',
  },
});

export default CladeAlert;
