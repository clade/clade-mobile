import React from 'react';
import {Text, StyleSheet} from 'react-native';

function HeaderH1(props) {
  const {h1} = props;

  return <Text style={[style.h1_text]}>{h1}</Text>;
}

const style = StyleSheet.create({
  h1_text: {
    textAlign: 'left',
    color: 'gray',
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default HeaderH1;
