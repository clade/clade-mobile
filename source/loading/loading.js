import React from 'react';
import {View} from 'react-native';
import globalStyles from '../../assets/stylesheet/global-stylesheet';
let color = globalStyles.color;

function Loading() {
  return <View style={{flex: 1, backgroundColor: color.blue}} />;
}

export default Loading();
