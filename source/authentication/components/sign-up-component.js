import React from 'react';
import {View, Text, StyleSheet, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';

const icon_size = 32;

function SignUpComponent(props) {
  const navigation = useNavigation();

  function navigateToSignUp(state) {
    if (!state) {
      navigation.navigate('SignUpScreen');
    }
  }

  return (
    <View style={style(props).view}>
      <Pressable
        style={style(props).pressable}
        onPress={() => navigateToSignUp(props.activated)}>
        <Icon name="user-plus" size={icon_size} color={props.color} />
        <Text style={style(props).text}>Sign Up</Text>
      </Pressable>
    </View>
  );
}

const style = props =>
  StyleSheet.create({
    view: {
      width: 60,
      alignItems: 'center',
    },
    pressable: {
      alignItems: 'center',
    },
    text: {
      marginTop: 5,
      color: props.color,
      fontSize: 14,
    },
  });

export default SignUpComponent;
