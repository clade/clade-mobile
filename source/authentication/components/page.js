import React from 'react';
import {View, Text, ActivityIndicator} from 'react-native';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../../../assets/selection.json';
import globalStyles from '../../../assets/stylesheet/global-stylesheet';
import Notify from './notify';
const IconMoon = createIconSetFromIcoMoon(
  icoMoonConfig,
  'icomoon',
  'icomoon.ttf',
);

const color = globalStyles.color;

const notifications = (ok) => {
  if (ok === true) {
    return <Notify />;
  }
};

const Page = ({
  backgroundColor,
  iconName,
  title,
  subTitle,
  icons = [],
  OKNotifications = false,
}) => {
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        backgroundColor,
        padding: 35,
        textAlign: 'center',
      }}>
      <IconMoon
        style={{marginTop: 10}}
        name={iconName}
        size={100}
        color="white"
      />

      <View
        style={{
          marginTop: 30,
          borderBottomColor: color.pink,
          borderBottomWidth: 2,
        }}>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 22,
            fontWeight: 'bold',
            color: 'white',
          }}>
          {title}
        </Text>
      </View>
      <View style={{marginTop: 14}}>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 18,
            fontWeight: 'bold',
            color: 'white',
          }}>
          {subTitle}
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          backgroundColor: color.blue,
          alignItems: 'center',
        }}>
        {icons.map((icon) => {
          return (
            <IconMoon
              key={icon.toString()}
              style={{marginTop: 10, padding: 20}}
              name={icon}
              size={30}
              color="white"
            />
          );
        })}
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          backgroundColor: color.blue,
          alignItems: 'center',
        }}>
        {notifications(OKNotifications)}
      </View>
    </View>
  );
};

export default Page;
