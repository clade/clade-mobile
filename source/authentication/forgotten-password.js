import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import Amplify, {Auth} from 'aws-amplify';
import awsconfig from '../../aws-exports';
import CladeAlert from '../components/clade-alert';
import globalStyles from '../../assets/stylesheet/global-stylesheet';
import ImageHelper from '../helpers/image-helper';
Amplify.configure(awsconfig);
import {useNavigation} from '@react-navigation/native';

let color = globalStyles.color;

function ForgottenPasswordScreen() {
  const navigation = useNavigation();

  const [email, setEmail] = useState('');
  const [forgotSpinner, setForgotSpinner] = useState(false);
  const [forgotAlert, setForgotAlert] = useState({status: false, message: ''});

  const validateEmail = (email) => {
    const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

    return expression.test(String(email).toLowerCase());
  };

  const checkUserInput = () => {
    if (!email.trim()) {
      setForgotAlert({status: true, message: 'Please enter an email'});
      return;
    }

    if (!validateEmail(email)) {
      setForgotAlert({status: true, message: 'Please enter a valid email'});
      return;
    }

    passwordReset().done();
  };

  async function passwordReset() {
    setForgotSpinner(true);
    Auth.forgotPassword(email)
      .then((user) => {
        setForgotSpinner(false);
        navigation.navigate('PasswordResetScreen', {email: email});
      })
      .catch((err) => {
        const {message} = err;
        console.log(message);
      });
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#1a1e34',
        padding: 30,
        paddingTop: 10,
      }}>
      <Image
        style={{height: 90, width: 70}}
        source={ImageHelper.cladeSmall()}
      />

      <Text style={{color: 'gray', fontSize: 24, marginTop: 20}}>
        Please enter the email you signed up with.
      </Text>
      <TextInput
        style={style.text_input}
        onChangeText={(text) => setEmail(text)}
        placeholder="Email"
        value={email}
        autoCompleteType="off"
        username
        autoCapitalize="none"
        underlineColorAndroid="transparent"
        placeholderTextColor="white"
      />

      <View style={style.sign_in_view}>
        <TouchableOpacity
          style={style.forgotten_password_button}
          onPress={() => checkUserInput()}>
          <Text style={style.sign_in_text}>Submit</Text>
        </TouchableOpacity>
      </View>

      <View style={style.spinner}>
        <ActivityIndicator
          size="large"
          color="#fff"
          animating={forgotSpinner}
        />
      </View>

      <CladeAlert
        title="Forgot Password"
        alert={forgotAlert}
        onConfirmPressed={() => {
          setForgotAlert({status: false});
        }}
      />
    </View>
  );
}

const style = StyleSheet.create({
  sign_in_view: {
    alignItems: 'flex-start',
    marginTop: 30,
  },
  sign_in_text: {
    color: 'white',
    fontSize: 20,
  },
  text_input: {
    marginTop: 20,
    color: 'white',
    height: 50,
    paddingBottom: 20,
    borderBottomWidth: 2,
    borderBottomColor: color.pink,
  },
  forgotten_password_button: {
    borderRadius: 5,
    padding: 10,
    backgroundColor: color.pink,
  },
});

export default ForgottenPasswordScreen;
