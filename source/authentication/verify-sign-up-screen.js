import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import Amplify, {Auth} from 'aws-amplify';
import awsconfig from '../../aws-exports';
import CladeAlert from '../components/clade-alert';
import globalStyles from '../../assets/stylesheet/global-stylesheet';
import {useNavigation, StackActions} from '@react-navigation/native';
import ImageHelper from '../helpers/image-helper';
import {useRoute} from '@react-navigation/native';

Amplify.configure(awsconfig);
const color = globalStyles.color;

function VerifySignUpScreen() {
  const navigation = useNavigation();
  const [verifyCode, setVerifyCode] = useState('');
  const [verifySpinner, setVerifySpinner] = useState(false);
  const [verifyAlert, setVerifyAlert] = useState({status: false, message: ''});
  const route = useRoute();

  const checkUserInput = () => {
    if (!verifyCode.trim()) {
      setVerifyAlert({
        status: true,
        message: 'Please enter your verify number',
      });
      return;
    }

    const {username} = route.params;
    verify(username).done();
  };

  async function verify(userName) {
    setVerifySpinner(true);
    Auth.confirmSignUp(userName, verifyCode)
      .then((res) => {
        console.log('Confirmed Signed Up', res);
        setVerifySpinner(false);
        navigation.dispatch(
          StackActions.replace('WelcomeScreen', {
            title: 'Welcome',
          }),
        );
      })
      .catch((err) => {
        console.log('Error Confirming Failed', err);
        const {message} = err;
        setVerifySpinner(false);
        setVerifyAlert({status: true, message: message});
      });
  }

  return (
    <View style={style.container}>
      <Image style={style.clade_image} source={ImageHelper.cladeSmall()} />
      <Text style={style.verify_message}>Congratulations on Signing Up,</Text>
      <Text style={style.verify_to}>
        please verify with the code sent to your email
      </Text>

      <TextInput
        style={style.text_input}
        onChangeText={(text) => setVerifyCode(text)}
        placeholder="Verify code"
        value={verifyCode}
        autoCompleteType="off"
        autoCapitalize="none"
        underlineColorAndroid="transparent"
        placeholderTextColor="white"
      />

      <View style={style.verify_view}>
        <TouchableOpacity style={style.verify_button} onPress={checkUserInput}>
          <Text style={style.verify_text}>Verify</Text>
        </TouchableOpacity>
      </View>

      <View style={style.spinner}>
        <ActivityIndicator
          size="large"
          color="#fff"
          animating={verifySpinner}
        />
      </View>

      <CladeAlert
        title="Verify"
        alert={verifyAlert}
        onConfirmPressed={() => {
          setVerifyAlert({status: false});
        }}
      />
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.blue,
    padding: 50,
    paddingTop: 100,
  },
  clade_image: {
    height: 90,
    width: 70,
  },
  verify_message: {
    color: 'white',
    marginTop: 20,
    fontSize: 24,
  },
  verify_to: {
    color: 'gray',
    fontSize: 22,
    marginTop: 5,
  },
  text_input: {
    marginTop: 20,
    color: 'white',
    height: 50,
    paddingBottom: 20,
    borderBottomWidth: 2,
    borderBottomColor: color.pink,
    fontSize: 18,
  },
  verify_view: {
    alignItems: 'flex-start',
    marginTop: 30,
  },
  verify_text: {
    color: 'white',
    fontSize: 20,
  },
  spinner: {
    marginTop: 20,
  },
  verify_button: {
    borderRadius: 5,
    padding: 10,
    backgroundColor: color.pink,
  },
});

export default VerifySignUpScreen;
