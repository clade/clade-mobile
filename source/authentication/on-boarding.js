import React from 'react';
import {View} from 'react-native';
import globalStyles from '../../assets/stylesheet/global-stylesheet';
import PagerView from 'react-native-pager-view';
import config from '../../config';
import Footer from './components/footer';
import Page from './components/page';
import {useNavigation, StackActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

let color = globalStyles.color;

function OnBoarding() {
  const pager = React.createRef();
  const navigation = useNavigation();

  const onBoardingComplete = async () => {
    await AsyncStorage.setItem(config.on_boarding_key, JSON.stringify(true));
  };

  const letsGo = () => {
    onBoardingComplete().done();

    navigation.dispatch(
      StackActions.replace('SignInScreen', {
        title: 'Sign In',
      }),
    );
  };

  return (
    <View style={{flex: 1}}>
      <PagerView style={{flex: 1}} showPageIndicator ref={pager}>
        <View key="1">
          <Page
            backgroundColor={color.blue}
            iconName="clade"
            title="Welcome to Clade"
            subTitle="Bringing the future of AI to the retail investor."
          />

          <Footer
            backgroundColor={color.blue}
            rightButtonLabel="Next"
            rightButtonPress={() => pager.current?.setPage(1)}
          />
        </View>

        <View key="2">
          <Page
            backgroundColor={color.blue}
            iconName="coin-dollar"
            title="Clade an app for Investors with a difference"
            subTitle="Just like your favourite investor apps;
                        Clade also brings AI techniques to give you greater details
                        about companies, trends and financial details. Using
                        multiple sources, including social media to mainstream news."
            icons={['twitter', 'library', 'newspaper']}
          />

          <Footer
            backgroundColor={color.blue}
            leftButtonLabel="Back"
            leftButtonPress={() => pager.current?.setPage(0)}
            rightButtonLabel="Next"
            rightButtonPress={() => pager.current?.setPage(2)}
          />
        </View>

        <View key="3">
          <Page
            backgroundColor={color.blue}
            iconName="bell"
            title="Notifications"
            subTitle="If you wish for Clade to send you
                        notifications, such as price alerts, trending
                        news, then simply click OK."
            OKNotifications
          />

          <Footer
            backgroundColor={color.blue}
            leftButtonLabel="Back"
            leftButtonPress={() => pager.current?.setPage(1)}
            rightButtonLabel="Lets go!"
            rightButtonPress={() => letsGo()}
          />
        </View>
      </PagerView>
    </View>
  );
}

export default OnBoarding;
