import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  Platform,
} from 'react-native';
import SignNavComponent from './components/sign-nav';
import Amplify, {Auth} from 'aws-amplify';
import awsconfig from '../../aws-exports';
import CladeAlert from '../components/clade-alert';
import globalStyles from '../../assets/stylesheet/global-stylesheet';
import ImageHelper from '../helpers/image-helper';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../../assets/selection.json';
import PasswordInputText from 'react-native-hide-show-password-input';
import FingerprintScanner from 'react-native-fingerprint-scanner';
import AsyncStorage from '@react-native-async-storage/async-storage';
import config from '../../config';
import OS from '../helpers/os';

createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');
Amplify.configure(awsconfig);

let color = globalStyles.color;

function SignInScreen({navigation}) {
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [loginSpinner, setLoginSpinner] = useState(false);
  const [signInAlert, setSignInAlert] = useState({status: false, message: ''});

  function refreshTokenX(refreshToken) {
    return new Promise((resolve, reject) => {
      const data = {
        refreshToken, // the token from the provider
        expires_at: Date.now() * 100, // the timestamp for the expiration
      };
      resolve(data);
    });
  }

  useEffect(() => {
    const setup = async () => {
      const auth = await AsyncStorage.getItem(config.auth_key);

      if (auth === null) {
        return;
      }

      let auth_data = JSON.parse(auth);
      if (Object.keys(auth_data).length > 0) {
        console.log('auth_data', auth_data);

        const {signInUserSession} = auth_data;
        const {refreshToken} = signInUserSession;

        const currentConfig = Auth.configure({
          refreshHandlers: {
            clade: refreshTokenX(refreshToken),
          },
        });
        console.log(currentConfig);

        if (OS.ios()) {
          FingerprintScanner.isSensorAvailable()
            .then((type) => {
              let message = '';
              if (type === 'Face ID') {
                message = 'Scan your Face on the device to continue';
              } else {
                message =
                  'Scan your Fingerprint on the device scanner to continue';
              }

              if (type !== null && type !== undefined) {
                FingerprintScanner.authenticate({
                  description: message,
                })
                  .then(() => {
                    console.log('here!');
                    navigation.replace('Tabs');
                  })
                  .catch((error) => {
                    console.log('Authentication error is => ', error);
                  });
              }
            })
            .catch((error) =>
              console.log('isSensorAvailable error => ', error),
            );
        }
      } else {
        FingerprintScanner.isSensorAvailable()
          .then((type) => {
            console.log(type)
            if (currentAuthentication()) {
              authCurrent();
            }
          })
          .catch((err) => {
            console.log(err);
          });
      }
    };
    setup().done();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function currentAuthentication() {
    console.log(Platform.Version);
    return Platform.Version > 23;
  }

  function authCurrent() {
    FingerprintScanner.authenticate({title: 'Log in with Biometrics'})
      .then(() => {
        navigation.replace('Tabs');
      })
      .catch((error) => {
        console.warn('Error:', error);
      });
  }

  async function signIn() {
    setLoginSpinner(true);
    Auth.signIn(userName, password)
      .then((user) => {
        console.log(user);

        const writeUserData = async () => {
          await AsyncStorage.setItem(config.auth_key, JSON.stringify(user));
        };
        writeUserData().done();

        setLoginSpinner(false);
        navigation.replace('Tabs');
      })
      .catch((err) => {
        const {message} = err;
        setSignInAlert({status: true, message: message});
        setLoginSpinner(false);
      });
  }

  const checkUserInput = () => {
    if (!userName.trim()) {
      setSignInAlert({status: true, message: 'Please enter User name'});
      return;
    }

    if (!password.trim()) {
      setSignInAlert({status: true, message: 'Please enter a Password'});
      return;
    }

    signIn({navigation}).done();
  };

  return (
    <View style={style.container}>
      <TouchableOpacity onPress={() => navigation.replace('Tabs')}>
        <Image style={style.clade_image} source={ImageHelper.cladeSmall()} />
      </TouchableOpacity>
      <Text style={style.welcome_back}>Welcome back,</Text>
      <Text style={style.sign_in_to}>sign in to continue</Text>
      <View>
        <TextInput
          style={style.text_input}
          onChangeText={(text) => setUserName(text)}
          placeholder="User Name"
          value={userName}
          autoCompleteType="off"
          username
          autoCapitalize="none"
          underlineColorAndroid="transparent"
          placeholderTextColor="white"
        />
      </View>
      <View>
        <PasswordInputText
          style={style.password_text_input}
          onChangeText={(text) => setPassword(text)}
          placeholder="Password"
          defaultValue={password}
          placeholderTextColor="white"
          iconColor="white"
          label=""
          activeLineWidth={0}
          disabledLineWidth={0}
          lineWidth={0}
          textColor="white"
        />
      </View>
      <View style={style.sign_in_view}>
        <View style={style.sign_in_touch_view}>
          <TouchableOpacity
            style={style.sign_in_button}
            onPress={checkUserInput}>
            <Text style={style.sign_in_text}>Submit</Text>
          </TouchableOpacity>
        </View>
        <View style={style.forgot_touch_view}>
          <TouchableOpacity
            style={style.forgot_password_link}
            onPress={() => navigation.navigate('ForgottenPasswordScreen')}>
            <Text style={style.sign_in_text}>Forgot Password</Text>
          </TouchableOpacity>
        </View>
      </View>
      <SignNavComponent marginTop={50} sign_in={true} sign_out={false} />
      <View style={style.spinner}>
        <ActivityIndicator size="large" color="#fff" animating={loginSpinner} />
      </View>

      <CladeAlert
        title="Sign In"
        alert={signInAlert}
        onConfirmPressed={() => {
          setSignInAlert({status: false});
        }}
      />
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.blue,
    padding: 30,
    paddingTop: 10,
  },
  clade_image: {
    height: 90,
    width: 70,
  },
  welcome_back: {
    color: 'white',
    marginTop: 20,
    fontSize: 24,
  },
  sign_in_to: {
    color: 'gray',
    fontSize: 24,
  },
  text_input: {
    marginTop: 20,
    color: 'white',
    height: 50,
    paddingBottom: 20,
    borderBottomWidth: 2,
    borderBottomColor: color.pink,
    fontSize: 18,
  },
  sign_in_view: {
    marginTop: 30,
    flex: 1,
    flexDirection: 'row',
  },
  sign_in_text: {
    color: 'white',
    fontSize: OS.ios() ? 18 : 16,
  },
  sign_in_touch_view: {
    flex: 2,
    alignItems: 'flex-start',
  },
  forgot_touch_view: {
    flex: 2,
    alignItems: 'flex-end',
  },
  spinner: {
    marginTop: 20,
  },
  password_text_input: {
    color: 'white',
    borderBottomWidth: 2,
    borderBottomColor: color.pink,
    fontSize: 16,
  },
  sign_in_button: {
    borderRadius: 5,
    padding: 10,
    backgroundColor: color.pink,
  },
  forgot_password_link: {
    padding: 10,
  },
});

export default SignInScreen;
