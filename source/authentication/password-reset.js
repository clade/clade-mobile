import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import Amplify, {Auth} from 'aws-amplify';
import awsconfig from '../../aws-exports';
import CladeAlert from '../components/clade-alert';
import globalStyles from '../../assets/stylesheet/global-stylesheet';
import ImageHelper from '../helpers/image-helper';
import {useNavigation, useRoute} from '@react-navigation/native';
import PasswordInputText from 'react-native-hide-show-password-input';

Amplify.configure(awsconfig);
let color = globalStyles.color;

function PasswordResetScreen(props) {
  const navigation = useNavigation();
  const [resetCode, setResetCode] = useState('');
  const [password, setPassword] = useState('');
  const [repeatPassword, setRepeatPassword] = useState('');
  const [passwordResetAlert, setPasswordResetAlert] = useState({
    status: false,
    message: '',
  });
  const [passwordResetSpinner, setPasswordResetSpinner] = useState(false);
  const [successfulChangeAlert, setSuccessfulChangeAlert] = useState({
    status: false,
    message: '',
  });
  const route = useRoute();

  const validatePassword = (pass) => {
    const expression = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,30}$/;

    return expression.test(String(pass).toLowerCase());
  };

  const checkUserInput = () => {
    if (!resetCode.trim()) {
      setPasswordResetAlert({status: true, message: 'Please enter reset code'});
      return;
    }

    if (
      !password.trim() ||
      !repeatPassword.trim() ||
      validatePassword(password.trim())
    ) {
      setPasswordResetAlert({
        status: true,
        message:
          'Please enter an 8 digit Password with numbers, special chars, uppercase and lowercase values',
      });
      return;
    }

    if (password.trim() !== repeatPassword.trim()) {
      setPasswordResetAlert({status: true, message: 'Please do not match'});
      return;
    }

    resetPassword().done();
  };

  async function resetPassword() {
    setPasswordResetSpinner(true);
    const {email} = route.params;

    try {
      await Auth.forgotPasswordSubmit(email, resetCode, password);
      setPasswordResetSpinner(false);
      setSuccessfulChangeAlert({
        status: true,
        message: 'Password reset was successful',
      });
    } catch (err) {
      const {message} = err;
      setPasswordResetAlert({status: true, message: message});
      setPasswordResetSpinner(false);
    }
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: color.blue,
        padding: 30,
        paddingTop: 10,
      }}>
      <Image
        style={{height: 90, width: 70}}
        source={ImageHelper.cladeSmall()}
      />

      <Text style={{color: 'gray', fontSize: 24, marginTop: 20}}>
        Please enter the reset code, sent to your email and create a new
        password
      </Text>
      <>
        <TextInput
          style={style.text_input}
          onChangeText={(text) => setResetCode(text)}
          placeholder="Reset Code"
          value={resetCode}
          autoCompleteType="off"
          username
          autoCapitalize="none"
          underlineColorAndroid="transparent"
          placeholderTextColor="white"
        />
        <PasswordInputText
          style={style.password_text_input}
          onChangeText={(text) => setPassword(text)}
          placeholder="Password"
          defaultValue={password}
          placeholderTextColor="white"
          iconColor="white"
          label=""
          activeLineWidth={0}
          disabledLineWidth={0}
          lineWidth={0}
          textColor="white"
        />
        <PasswordInputText
          style={style.password_text_input}
          onChangeText={(text) => setRepeatPassword(text)}
          placeholder="Password"
          defaultValue={repeatPassword}
          placeholderTextColor="white"
          iconColor="white"
          label=""
          activeLineWidth={0}
          disabledLineWidth={0}
          lineWidth={0}
          textColor="white"
        />

        <View style={style.touch_view}>
          <TouchableOpacity
            style={style.reset_password_button}
            onPress={() => checkUserInput()}>
            <Text style={style.reset_password_text}>Reset Password</Text>
          </TouchableOpacity>
        </View>
      </>

      <View style={style.spinner}>
        <ActivityIndicator
          size="large"
          color="#fff"
          animating={passwordResetSpinner}
        />
      </View>

      <CladeAlert
        title="Reset Password"
        alert={passwordResetAlert}
        onConfirmPressed={() => {
          setPasswordResetAlert({status: false});
        }}
      />

      <CladeAlert
        title="Confirmed Reset"
        alert={successfulChangeAlert}
        onConfirmPressed={() => {
          setSuccessfulChangeAlert({status: false});
          navigation.navigate('SignInScreen');
        }}
      />
    </View>
  );
}

const style = StyleSheet.create({
  text_input: {
    marginTop: 20,
    color: 'white',
    height: 50,
    paddingBottom: 20,
    borderBottomWidth: 2,
    borderBottomColor: color.pink,
  },
  touch_view: {
    marginTop: 20,
    alignItems: 'flex-start',
  },
  reset_password_text: {
    color: 'white',
    fontSize: 20,
  },
  password_text_input: {
    color: 'white',
    borderBottomWidth: 2,
    borderBottomColor: color.pink,
    fontSize: 16,
  },
  reset_password_button: {
    borderRadius: 5,
    padding: 10,
    backgroundColor: color.pink,
  },
});

export default PasswordResetScreen;
