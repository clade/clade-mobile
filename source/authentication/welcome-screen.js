import React from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import globalStyles from '../../assets/stylesheet/global-stylesheet';
import ImageHelper from '../helpers/image-helper';

const color = globalStyles.color;

function WelcomeScreen({navigation}) {
  return (
    <View style={style.main_view}>
      <Text style={style.welcome_view}>Welcome</Text>

      <View style={style.image_view}>
        <Image style={style.clade_image} source={ImageHelper.cladeSmall()} />
      </View>
      <View style={style.congrats_view}>
        <Text style={style.congrats_text}>
          Congratulations on signing up to Clade, press continue to open your
          account.
        </Text>
      </View>
      <View style={style.continue_view}>
        <TouchableOpacity
          style={style.continue_button}
          onPress={() => navigation.replace('Tabs')}>
          <Text style={style.continue_text}>Continue</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const style = StyleSheet.create({
  main_view: {
    flex: 1,
    backgroundColor: color.blue,
    padding: 50,
    paddingTop: 100,
  },
  welcome_view: {
    color: 'white',
    marginTop: 20,
    fontSize: 28,
    textAlign: 'center',
  },
  image_view: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  clade_image: {
    height: 200,
    width: 145,
  },
  congrats_view: {
    marginTop: 30,
  },
  congrats_text: {
    color: 'gray',
    fontSize: 24,
    textAlign: 'center',
  },
  continue_view: {
    alignItems: 'center',
    marginTop: 30,
  },
  continue_text: {
    color: 'white',
    fontSize: 26,
  },
  continue_button: {
    borderRadius: 5,
    padding: 10,
    backgroundColor: color.pink,
  },
});

export default WelcomeScreen;
