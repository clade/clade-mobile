import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Switch, Button, ScrollView} from 'react-native';
import globalStyles from '../assets/stylesheet/global-stylesheet';
import config from '../config';
import AsyncStorage from '@react-native-async-storage/async-storage';

let color = globalStyles.color;

//todo: This needs a re-write!

function SettingsScreen() {
  const [isPush, setIsPushEnabled] = useState(false);
  const [isEmail, setIsEmailEnabled] = useState(false);
  const [
    isPortfolioDeleteConfirmation,
    setIsPortfolioDeleteConfirmation,
  ] = useState(false);
  const [
    isWishListDeleteConfirmation,
    setIsWishListDeleteConfirmation,
  ] = useState(false);

  const toggleSwitch = () => {
    updateSettings({push_notifications: !isPush}).done();
    setIsPushEnabled(isPush => !isPush);
  };

  const toggleEmailSwitch = () => {
    updateSettings({email_notifications: !isEmail}).done();
    setIsEmailEnabled(isEmail => !isEmail);
  };
  const togglePortfolioSwitch = () => {
    updateSettings({
      portfolio_delete_confirmation: !isPortfolioDeleteConfirmation,
    }).done();
    setIsPortfolioDeleteConfirmation(
      isPortfolioDeleteConfirmation => !isPortfolioDeleteConfirmation,
    );
  };
  const toggleWishListSwitch = () => {
    updateSettings({
      wish_list_delete_confirmation: !isWishListDeleteConfirmation,
    }).done();
    setIsWishListDeleteConfirmation(
      isWishListDeleteConfirmation => !isWishListDeleteConfirmation,
    );
  };

  useEffect(() => {
    getSettings().done();
  }, []);

  const updateSettings = async key => {
    const settings_key = await AsyncStorage.getItem(config.settings_key);
    let settings = JSON.parse(settings_key);
    let updated_setting = Object.assign({}, settings, key);

    await AsyncStorage.setItem(
      config.settings_key,
      JSON.stringify(updated_setting),
    );
  };

  const getSettings = async () => {
    const settings_key = await AsyncStorage.getItem(config.settings_key);
    let settings = JSON.parse(settings_key);
    const {
      email_notifications,
      portfolio_delete_confirmation,
      push_notifications,
      wish_list_delete_confirmation,
    } = settings;
    setIsEmailEnabled(email_notifications);
    setIsPortfolioDeleteConfirmation(portfolio_delete_confirmation);
    setIsPushEnabled(push_notifications);
    setIsWishListDeleteConfirmation(wish_list_delete_confirmation);
  };

  async function clearRecentlyViewed() {
    const recentlyViewedKey = await AsyncStorage.getItem(
      config.recently_viewed_key,
    );
    if (recentlyViewedKey !== null) {
      await AsyncStorage.setItem(
        config.recently_viewed_key,
        JSON.stringify([]),
      );
      alert('Recently Reviewed Cleared');
    }
  }

  return (
    <ScrollView style={style.container}>
      <View
        style={{
          paddingBottom: 5,
          borderBottomWidth: 1,
          borderBottomColor: 'pink',
        }}>
        <Text style={{color: 'white', fontSize: 24}}>App Settings</Text>
      </View>

      <View style={{flex: 1, marginTop: 10}}>
        <View style={{alignSelf: 'stretch', flexDirection: 'row'}}>
          <View style={{marginTop: 10, flex: 1, alignItems: 'flex-start'}}>
            <Text style={{fontSize: 17, color: 'white', fontWeight: 'bold'}}>
              Push notifications
            </Text>
          </View>

          <View style={{marginTop: 15, flex: 0.3, alignItems: 'flex-end'}}>
            <Switch
              trackColor={{false: '#ebf0f2', true: '#767577'}}
              thumbColor={isPush ? '#ebf0f2' : '#f4140f'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={isPush}
            />
          </View>
        </View>

        <View style={{alignSelf: 'stretch', flexDirection: 'row'}}>
          <View style={{marginTop: 15, flex: 1, alignItems: 'flex-start'}}>
            <Text style={{fontSize: 17, color: 'white', fontWeight: 'bold'}}>
              Emails
            </Text>
          </View>

          <View style={{marginTop: 15, flex: 0.3, alignItems: 'flex-end'}}>
            <Switch
              trackColor={{false: '#ebf0f2', true: '#767577'}}
              thumbColor={isEmail ? '#ebf0f2' : '#f4140f'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleEmailSwitch}
              value={isEmail}
            />
          </View>
        </View>

        <View style={{alignSelf: 'stretch', flexDirection: 'row'}}>
          <View style={{marginTop: 25, flex: 1, alignItems: 'flex-start'}}>
            <Text style={{fontSize: 17, color: 'white', fontWeight: 'bold'}}>
              Recently Reviewed
            </Text>
          </View>

          <View style={{alignSelf: 'center'}}>
            <Button
              onPress={() => clearRecentlyViewed()}
              title="Clear"
              color={color.pink}
            />
          </View>
        </View>

        <View style={{alignSelf: 'stretch', flexDirection: 'row'}}>
          <View style={{marginTop: 25, flex: 1, alignItems: 'flex-start'}}>
            <Text style={{fontSize: 17, color: 'white', fontWeight: 'bold'}}>
              Portfolio Delete Confirmation
            </Text>
          </View>

          <View style={{marginTop: 20, flex: 0.3, alignItems: 'flex-end'}}>
            <Switch
              trackColor={{false: '#ebf0f2', true: '#767577'}}
              thumbColor={isPortfolioDeleteConfirmation ? '#ebf0f2' : '#f4140f'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={togglePortfolioSwitch}
              value={isPortfolioDeleteConfirmation}
            />
          </View>
        </View>

        <View style={{alignSelf: 'stretch', flexDirection: 'row'}}>
          <View style={{marginTop: 25, flex: 1, alignItems: 'flex-start'}}>
            <Text style={{fontSize: 17, color: 'white', fontWeight: 'bold'}}>
              Wishlist Delete Confirmation
            </Text>
          </View>

          <View style={{marginTop: 20, flex: 0.3, alignItems: 'flex-end'}}>
            <Switch
              trackColor={{false: '#ebf0f2', true: '#767577'}}
              thumbColor={isWishListDeleteConfirmation ? '#ebf0f2' : '#f4140f'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleWishListSwitch}
              value={isWishListDeleteConfirmation}
            />
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1a1e34',
    padding: 10,
  },
});

export default SettingsScreen;
