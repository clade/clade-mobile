import React, {useEffect, useState} from 'react';
import {
  View,
  ActivityIndicator,
  StyleSheet,
  ScrollView,
  Text,
  Modal,
  Alert,
  Pressable,
  TextInput,
} from 'react-native';
import PriceChart from './components/price-chart';
import Chart52Week from './components/chart52-week';
import RecommendedChart from './components/recommended-chart';
import CompanyFinanceInfo from './components/company-info';
import CompanyNews from './components/company-news';
import globalStyles from '../../assets/stylesheet/global-stylesheet';
import Icon from 'react-native-vector-icons/Feather';
import axios from 'axios';
import Site from '../helpers/site-helper';
import LogoHelper from '../helpers/logo-helper';
import config from '../../config';
import Image from 'react-native-image-progress';
import CircleSnail from 'react-native-progress/CircleSnail';
import Similar from './components/similar';
import {useNavigation, useRoute} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

let color = globalStyles.color;

function CompanyDetailsScreen() {
  const [modalVisible, setModalVisible] = useState(false);
  const [modalPortfolio, setModalPortfolio] = useState(false);
  const [noShares, setNoShares] = useState(0);
  const [pricePaidPerShare, setPricePaidPerShare] = useState(0);
  const [companyDetails, setCompanyDetails] = useState('');
  const [loading, setLoading] = useState(true);

  const navigation = useNavigation();
  const route = useRoute();
  const {symbol} = route.params;

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Text
          onPress={() => openWatchList()}
          style={{
            color: 'white',
            fontWeight: 'bold',
            fontSize: 28,
            marginRight: 10,
          }}>
          +
        </Text>
      ),
      title: symbol,
    });

    const fetchCompanyDetails = async () => {
      const result = await axios(
        `${Site.getURL()}/company_details?symbol=${symbol}`,
      );

      setCompanyDetails(result.data);
      setLoading(false);
    };

    fetchCompanyDetails().done();
    addRecentlyViewed().done();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  async function addRecentlyViewed() {
    const recentlyViewedKey = await AsyncStorage.getItem(
      config.recently_viewed_key,
    );
    if (recentlyViewedKey !== null) {
      let recentlyViewed = JSON.parse(recentlyViewedKey);
      if (!recentlyViewed.includes(symbol)) {
        if (recentlyViewed.length >= 5) {
          recentlyViewed.splice(0, 1);
        }

        recentlyViewed.push(symbol);
        await AsyncStorage.setItem(
          config.recently_viewed_key,
          JSON.stringify(recentlyViewed),
        );
      }
    }
  }

  const createPortfolioObject = () => {
    return {
      symbol: symbol,
      stock: {
        no_shares: parseFloat(noShares),
        price_per_share: parseFloat(pricePaidPerShare),
      },
    };
  };

  async function addToPortfolio() {
    try {
      const portfolioKey = await AsyncStorage.getItem(config.portfolio_key);
      if (portfolioKey !== null) {
        let portfolio = JSON.parse(portfolioKey);
        if (portfolio.findIndex((stock) => stock.symbol === symbol) === -1) {
          portfolio.push(createPortfolioObject());
          await AsyncStorage.setItem(
            config.portfolio_key,
            JSON.stringify(portfolio),
          );
        }
      }

      setModalPortfolio(!modalPortfolio);
    } catch (error) {
      setModalPortfolio(!modalPortfolio);
    }
  }

  async function addToWatchlist() {
    try {
      const watchListKey = await AsyncStorage.getItem(config.wish_list_key);
      if (watchListKey !== null) {
        let watchLists = JSON.parse(watchListKey);
        if (!watchLists.includes(symbol)) {
          watchLists.push(symbol);
          await AsyncStorage.setItem(
            config.wish_list_key,
            JSON.stringify(watchLists),
          );
        }
      }
      setModalVisible(!modalVisible);
    } catch (error) {
      setModalVisible(!modalVisible);
    }
  }

  function openWatchList() {
    setModalVisible(true);
  }

  function portfolioOpen() {
    setModalVisible(!modalVisible);
    setModalPortfolio(!modalPortfolio);
  }

  const getHeader = () => {
    if (loading) {
      return (
        <View
          style={{marginTop: 10, marginLeft: 100, alignItems: 'flex-start'}}>
          <ActivityIndicator
            animating={loading}
            size="small"
            color={color.pink}
          />
        </View>
      );
    }
    return (
      <View>
        <Text style={{color: 'white', fontSize: 24}}>
          {companyDetails.name}
        </Text>
        <Text style={{color: 'white', fontSize: 14}}>
          {companyDetails.industry}
        </Text>
      </View>
    );
  };

  return (
    <View style={style.container}>
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={style.centeredView}>
          <View style={style.modalView}>
            <View style={{alignSelf: 'flex-end'}}>
              <Icon
                onPress={() => setModalVisible(!modalVisible)}
                name="x-square"
                size={32}
                color={color.pink}
              />
            </View>
            <Text style={style.modalText}>
              Select which list to save {companyDetails.name} to.
            </Text>
            <View style={{flexDirection: 'row'}}>
              <Pressable
                style={[style.button, style.buttonClose]}
                onPress={() => portfolioOpen()}>
                <Text style={style.textStyle}>Portfolio</Text>
              </Pressable>

              <Pressable
                style={[style.button, style.buttonClose]}
                onPress={() => addToWatchlist()}>
                <Text style={style.textStyle}>Watchlist</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalPortfolio}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalPortfolio(!modalPortfolio);
        }}>
        <View style={style.centeredView}>
          <View style={style.modalView}>
            <View style={{alignSelf: 'flex-end'}}>
              <Icon
                onPress={() => setModalPortfolio(!modalPortfolio)}
                name="x-square"
                size={32}
                color={color.pink}
              />
            </View>
            <Text style={style.modalText}>Enter Portfolio amount</Text>
            <View>
              <View style={{flexDirection: 'row'}}>
                <View style={{width: 150, margin: 10}}>
                  <TextInput
                    keyboardType="numeric"
                    style={{
                      padding: 5,
                      fontSize: 14,
                      color: color.dark_gray,
                      height: 40,
                      borderColor: 'gray',
                      borderWidth: 1,
                    }}
                    onChangeText={(text) => setNoShares(text)}
                    value={noShares}
                    placeholder="Share quantity"
                  />
                </View>
                <View style={{width: 150, margin: 10}}>
                  <TextInput
                    keyboardType="numeric"
                    style={{
                      padding: 5,
                      fontSize: 14,
                      color: color.dark_gray,
                      height: 40,
                      borderColor: 'gray',
                      borderWidth: 1,
                    }}
                    onChangeText={(text) => setPricePaidPerShare(text)}
                    value={pricePaidPerShare}
                    placeholder="Price per/share"
                  />
                </View>
              </View>

              <View style={{alignSelf: 'flex-start', marginRight: 50}}>
                <Text style={{paddingLeft: 10, fontSize: 22}}>
                  Price Paid ($): {(noShares * pricePaidPerShare).toFixed(2)}
                </Text>
              </View>

              <Pressable
                style={[style.button, style.buttonClose]}
                onPress={() => addToPortfolio()}>
                <Text style={style.textStyle}>Save</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>

      <ScrollView
        showsVerticalScrollIndicator={false}
        style={style.scroll_view}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            paddingBottom: 10,
            borderBottomWidth: 1,
            borderBottomColor: color.pink,
          }}>
          <View style={{flex: 1}}>{getHeader()}</View>
          <Image
            indicator={CircleSnail}
            indicatorProps={{
              size: 30,
              color: 'rgb(253,93,147)',
              unfilledColor: 'rgba(200, 200, 200, 0.2)',
            }}
            source={{uri: LogoHelper.get(companyDetails.url)}}
            style={{height: 50, width: 50}}
            duration={1000}
            spinDuration={5000}
            imageStyle={{
              borderRadius: 5,
            }}
          />
        </View>

        <PriceChart symbol={symbol} />
        <CompanyFinanceInfo symbol={symbol} />
        <Chart52Week symbol={symbol} />
        {/*<RecommendedChart />*/}
        <CompanyNews symbol={symbol} />
        <Similar symbol={symbol} />
      </ScrollView>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#1a1e34',
  },
  scroll_view: {
    marginTop: 10,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 80,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 5,
    margin: 10,
    padding: 20,
    elevation: 2,
  },
  buttonClose: {
    backgroundColor: color.blue,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 22,
  },
  modalText: {
    width: 300,
    fontSize: 22,
    marginBottom: 15,
    textAlign: 'center',
    color: color.dark_gray,
  },
});

export default CompanyDetailsScreen;
