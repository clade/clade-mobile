import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, TextInput, StyleSheet, Image} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import axios from 'axios';
import Site from '../helpers/site-helper';
import {TouchableOpacity} from 'react-native-gesture-handler';
import LogoHelper from '../helpers/logo-helper';
import globalStyles from '../../assets/stylesheet/global-stylesheet';

let color = globalStyles.color;

function CompanySearchByIndustry() {
  const navigation = useNavigation();
  const route = useRoute();

  const [filteredDataSource, setFilteredDataSource] = useState([]);
  const [masterDataSource, setMasterDataSource] = useState([]);
  const [search, setSearch] = useState('');

  useEffect(() => {
    console.log('Route:', route.params);

    const {industry_data} = route.params;
    navigation.setOptions({title: industry_data.industry});

    const fetchByIndustry = async () => {
      const result = await axios(
        `${Site.getURL()}/companies_by_industry?industry_id=${
          industry_data.industry_id
        }`,
      );

      setFilteredDataSource(result.data);
      setMasterDataSource(result.data);
    };

    fetchByIndustry().done();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderIndustry = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() =>
          navigation.push('CompanyDetailsScreen', {symbol: item.symbol})
        }
        style={{
          marginBottom: 5,
          flexDirection: 'row',
          height: 50,
          borderWidth: 1,
          borderColor: color.blue,
          borderBottomColor: color.pink,
        }}>
        <View style={{flex: 0.1}}>
          <Image
            style={{borderRadius: 5, height: 30, width: 30}}
            source={{
              uri: LogoHelper.get(item.company_data[0].url),
            }}
          />
        </View>
        <View style={{flex: 0.6}}>
          <Text style={{marginTop: 10, color: 'white', fontSize: 16}}>
            {item.company_data[0].name}
          </Text>
        </View>
        <View style={{flex: 0.3}}>
          <Text
            style={{
              textAlign: 'right',
              marginTop: 10,
              color: color.stock_green,
              fontSize: 16,
            }}>
            {item.percent_change}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const searchFilterFunction = (text) => {
    // Check if searched text is not blank
    if (text) {
      // Inserted text is not blank
      // Filter the masterDataSource
      // Update FilteredDataSource
      const newData = masterDataSource.filter(function (item) {
        const itemData = item.name ? item.name.toUpperCase() : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
      setSearch(text);
    } else {
      // Inserted text is blank
      // Update FilteredDataSource with masterDataSource
      setFilteredDataSource(masterDataSource);
      setSearch(text);
    }
  };

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.textInputStyle}
        onChangeText={(text) => searchFilterFunction(text)}
        value={search}
        underlineColorAndroid="transparent"
        placeholder="Search Here"
      />
      <FlatList
        showsHorizontalScrollIndicator={false}
        legacyImplementation={false}
        data={filteredDataSource}
        renderItem={(item) => renderIndustry(item)}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#1a1e34',
  },
  itemStyle: {
    padding: 10,
  },
  textInputStyle: {
    height: 40,
    borderWidth: 1,
    padding: 10,
    borderColor: color.pink,
    backgroundColor: '#FFFFFF',
    marginBottom: 20,
  },
});

export default CompanySearchByIndustry;
