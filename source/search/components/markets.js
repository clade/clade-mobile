import React, {useEffect, useState} from 'react';
import globalStyles from '../../../assets/stylesheet/global-stylesheet';
import {FlatList, Text, View} from 'react-native';
import axios from 'axios';
import Site from '../../helpers/site-helper';

let color = globalStyles.color;

function Markets() {
  const [markets, setMarkets] = useState([]);

  useEffect(() => {
    const fetchMarkets = async () => {
      const result = await axios(`${Site.getURL()}/markets`);
      setMarkets(result.data);
    };

    fetchMarkets().done();
  }, []);

  const getName = item => {
    try {
      return item.market_data[0].name;
    } catch (e) {
      return item.symbol;
    }
  };

  const renderMarket = ({item}) => {
    return (
      <View>
        <View
          style={{
            marginBottom: 5,
            flexDirection: 'row',
            height: 50,
            borderWidth: 1,
            borderColor: color.blue,
            borderBottomColor: color.pink,
          }}>
          <View style={{flex: 0.5}}>
            <Text style={{marginTop: 10, color: 'white', fontSize: 16}}>
              {getName(item)}
            </Text>
          </View>
          <View style={{flex: 0.5}}>
            <Text
              style={{
                textAlign: 'right',
                marginTop: 10,
                color: color.stock_green,
                fontSize: 16,
              }}>
              {item.previous_close}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View>
      <View
        style={{
          paddingBottom: 5,
          borderBottomWidth: 1,
          borderBottomColor: 'pink',
        }}>
        <Text style={{color: 'white', fontSize: 24}}>Markets</Text>
      </View>

      <FlatList
        showsHorizontalScrollIndicator={false}
        legacyImplementation={false}
        data={markets}
        renderItem={item => renderMarket(item)}
        keyExtractor={item => item.symbol}
      />
    </View>
  );
}

export default Markets;
