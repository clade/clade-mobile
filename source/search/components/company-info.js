import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import HeaderH1 from '../../components/header-h1';
import Site from '../../helpers/site-helper';
import axios from 'axios';
import globalStyles from '../../../assets/stylesheet/global-stylesheet';

let color = globalStyles.color;

function CompanyFinanceInfo(props) {
  const [dividendsData, setDividendsData] = useState('');
  const [companyFundamentals, setCompanyFundamentals] = useState('');
  const [previousDay, setPreviousDay] = useState({
    open: 0,
    close: 0,
    change: '',
  });

  useEffect(() => {
    fetchCompanyDividends().done();
    fetchLastDaysTicker().done();
  }, []);

  function calculatePreviousChange(open, close) {
    let difference = open - close;
    let change = ((difference / open) * 100).toFixed(2);
    if (change < 0) {
      return `%${Math.abs(change)} (${difference.toFixed(2)})`;
    } else {
      return `%${change} (${difference.toFixed(2)})`;
    }
  }

  const fetchLastDaysTicker = async () => {
    try {
      const result = await axios(
        `${Site.getURL()}/ticker_previous_day?symbol=${props.symbol}`,
      );
      const {open, close} = result.data.values;

      let open_float = parseFloat(open);
      let close_float = parseFloat(close);
      setPreviousDay({
        open: open_float,
        close: close_float,
        change: calculatePreviousChange(open_float, close_float),
      });
    } catch {}
  };

  const fetchCompanyDividends = async () => {
    try {
      const result = await axios(
        `${Site.getURL()}/dividends?symbol=${props.symbol}`,
      );
      setDividendsData(result.data);
    } catch (error) {
      setDividendsData({
        dividend_yield: 0,
      });
    }
  };

  function calc_dividend_yield(dividend) {
    if (dividend) {
      return (dividend * 100).toFixed(2);
    } else {
      return 'N/A';
    }
  }

  useEffect(() => {
    const fetchCompanyFundamentals = async () => {
      const result = await axios(
        `${Site.getURL()}/company?symbol=${props.symbol}`,
      );

      setCompanyFundamentals(result.data);
    };

    fetchCompanyFundamentals().done();
  }, []);

  return (
    <View style={{marginBottom: 20}}>
      <HeaderH1 h1="Overview" />
      <View style={style.container}>
        <View style={{flexDirection: 'row', marginTop: 10}}>
          <View style={{flex: 2}}>
            <Text style={{color: 'white', fontSize: 16}}>Dividend yield</Text>
          </View>
          <Text style={{color: 'white', fontSize: 16}}>
            {calc_dividend_yield(dividendsData.dividend_yield)}%
          </Text>
        </View>

        <View style={{flexDirection: 'row', marginTop: 10}}>
          <View style={{flex: 2}}>
            <Text style={{color: 'white', fontSize: 16}}>PE Ratio</Text>
          </View>
          <Text style={{color: 'white', fontSize: 16}}>
            {companyFundamentals.pe_ratio}
          </Text>
        </View>

        <View style={{flexDirection: 'row', marginTop: 10}}>
          <View style={{flex: 2}}>
            <Text style={{color: 'white', fontSize: 16}}>Market Cap</Text>
          </View>
          <Text style={{color: 'white', fontSize: 16}}>
            ${companyFundamentals.market_cap}
          </Text>
        </View>

        <View style={{flexDirection: 'row', marginTop: 10}}>
          <View style={{flex: 2}}>
            <Text style={{color: 'white', fontSize: 16}}>Volume</Text>
          </View>
          <Text style={{color: 'white', fontSize: 16}}>
            {companyFundamentals.volume}
          </Text>
        </View>

        <View style={{flexDirection: 'row', marginTop: 10}}>
          <View style={{flex: 2}}>
            <Text style={{color: 'white', fontSize: 16}}>Previous close</Text>
          </View>
          <Text style={{color: 'white', fontSize: 16}}>
            {previousDay.close}
          </Text>
        </View>

        <View style={{flexDirection: 'row', marginTop: 10}}>
          <View style={{flex: 2}}>
            <Text style={{color: 'white', fontSize: 16}}>Previous change</Text>
          </View>
          <Text
            style={{
              color: previousDay.change.includes('-')
                ? color.stock_red
                : color.stock_green,
              fontSize: 16,
            }}>
            {previousDay.change}
          </Text>
        </View>
      </View>
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    marginTop: 10,
  },
});

export default CompanyFinanceInfo;
