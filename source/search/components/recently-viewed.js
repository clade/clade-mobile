import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, TouchableHighlight} from 'react-native';
import globalStyles from '../../../assets/stylesheet/global-stylesheet';
import RecentlyViewedDetails from './recently-viewed-details';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import config from '../../../config';
import AsyncStorage from '@react-native-async-storage/async-storage';

let color = globalStyles.color;

function RecentlyViewed() {
  const navigation = useNavigation();
  const [reviewData, setReviewData] = useState([]);
  const isFocused = useIsFocused();

  useEffect(() => {
    if (isFocused) {
      recentlyViewed().done();
    }
  }, [isFocused]);

  const recentlyViewed = async () => {
    const recentlyViewedKey = await AsyncStorage.getItem(
      config.recently_viewed_key,
    );
    if (recentlyViewedKey !== null) {
      setReviewData(JSON.parse(recentlyViewedKey).reverse());
    }
  };

  const renderItem = ({item}) => (
    <TouchableHighlight
      onPress={() =>
        navigation.navigate('CompanyDetailsScreen', {title: item, symbol: item})
      }
      style={{
        borderRadius: 10,
        height: 120,
        width: 160,
        backgroundColor: color.dark_gray,
        marginRight: 20,
      }}>
      <RecentlyViewedDetails symbol={item} />
    </TouchableHighlight>
  );

  const footer = () => {
    if (reviewData.length > 0) {
      return <View style={{marginBottom: 10}} />;
    }
  };

  const header = () => {
    if (reviewData.length > 0) {
      return (
        <View
          style={{
            marginTop: 35,
            marginBottom: 10,
            paddingBottom: 5,
            borderBottomWidth: 1,
            borderBottomColor: 'pink',
          }}>
          <Text style={{color: 'white', fontSize: 24}}>Recently Viewed</Text>
        </View>
      );
    }
  };

  return (
    <View>
      {header()}
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        legacyImplementation={false}
        data={reviewData}
        renderItem={item => renderItem(item)}
        keyExtractor={item => item}
      />
      {footer()}
    </View>
  );
}

export default RecentlyViewed;
