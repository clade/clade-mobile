import React, {useEffect, useState} from 'react';
import {VictoryAxis, VictoryChart, VictoryLine} from 'victory-native';
import {Text, View, TouchableOpacity} from 'react-native';
import axios from 'axios';
import Site from '../../helpers/site-helper';
import globalStyles from '../../../assets/stylesheet/global-stylesheet';

let color = globalStyles.color;

const ChartOptions = Object.freeze({
  '1day': {limit: 16, type: '30min'},
  '5day': {limit: 5, type: '1day'},
  '1month': {limit: 30, type: '1day'},
  '3month': {limit: 30 * 3, type: '1week'},
  '6month': {limit: 6, type: '1month'},
  '1year': {limit: 12, type: '1month'},
  '5year': {limit: 60, type: '1month'},
});

function PriceChart(props) {
  const [tickerData, setTickerData] = useState([{index: 0, closed: 0}]);
  const [minDomain, setMinDomain] = useState(0);
  const [maxDomain, setMaxDomain] = useState(0);
  const [tickOption, setTickOption] = useState('1day');

  useEffect(() => {
    let options = ChartOptions['1month'];
    fetchTicker(props.symbol, options).done();
  }, []);

  const fetchTicker = async (symbol, ticker_options) => {
    const {limit, type} = ticker_options;

    const result = await axios(
      `${Site.getURL()}/ticker?symbol=${symbol}&ticker_type=${type}&limit=${limit}`,
    );

    let tick_data = result.data.map((tick, index) => {
      return {
        index: index,
        open: parseFloat(tick.values.close),
      };
    });

    setMax(tick_data);
    setMin(tick_data);
    setTickerData(tick_data);
  };

  function setMin(tick_data) {
    if (tick_data.length > 0) {
      let closed_values = tick_data.map(td => {
        return td.open;
      });
      let smallest_value = Math.min(...closed_values);
      let max_height = smallest_value * 0.9;
      let set_threshold = Math.ceil(max_height / 10) * 10;
      setMinDomain(set_threshold);
    }
  }

  function setMax(tick_data) {
    if (tick_data.length > 0) {
      let closed_values = tick_data.map(td => {
        return td.open;
      });
      let largest_value = Math.max(...closed_values);
      let max_height = largest_value * 1.1;
      let set_threshold = Math.ceil(max_height / 10) * 10;
      setMaxDomain(set_threshold);
    }
  }

  const getTickColor = tick => {
    if (tickOption === tick) {
      return 'white';
    }
    return 'gray';
  };

  const getTickerData = type => {
    setTickOption(type);
    fetchTicker(props.symbol, ChartOptions[type]).done();
  };

  return (
    <>
      <VictoryChart
        width={400}
        height={300}
        minDomain={{y: minDomain}}
        maxDomain={{y: maxDomain}}
        style={{
          labels: {
            fontSize: 15,
            fill: '#c43a31',
            padding: 15,
          },
        }}>
        <VictoryAxis
          dependentAxis
          style={{
            axis: {stroke: '#756f6a', strokeWidth: 2},
            grid: {stroke: 'transparent'},
            tickLabels: {fontSize: 13, fill: '#fff'},
            axisLabel: {fill: '#fff'},
          }}
        />

        <VictoryAxis
          style={{
            axis: {stroke: '#756f6a', strokeWidth: 2},
            grid: {stroke: 'transparent'},
            tickLabels: {fontSize: 0, fill: 'transparent'},
          }}
        />

        <VictoryLine
          style={{
            data: {stroke: 'white'},
            labels: {
              fontSize: 15,
              fill: '#c43a31',
              padding: 15,
            },
          }}
          data={tickerData}
          x="index"
          y="open"
          interpolation="natural"
        />
      </VictoryChart>

      <View
        style={{
          marginBottom: 20,
          flex: 1,
          alignSelf: 'stretch',
          flexDirection: 'row',
        }}>
        <TouchableOpacity
          onPress={() => getTickerData('1day')}
          style={{
            height: 40,
            borderWidth: 1,
            borderColor: color.pink,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{fontSize: 16, color: getTickColor('1day')}}>1d</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => getTickerData('5day')}
          style={{
            height: 40,
            borderWidth: 1,
            borderColor: color.pink,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{fontSize: 16, color: getTickColor('5day')}}>5d</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => getTickerData('1month')}
          style={{
            height: 40,
            borderWidth: 1,
            borderColor: color.pink,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{fontSize: 16, color: getTickColor('1month')}}>1m</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => getTickerData('3month')}
          style={{
            height: 40,
            borderWidth: 1,
            borderColor: color.pink,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{fontSize: 16, color: getTickColor('3month')}}>3m</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => getTickerData('6month')}
          style={{
            height: 40,
            borderWidth: 1,
            borderColor: color.pink,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{fontSize: 16, color: getTickColor('6month')}}>6m</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => getTickerData('1year')}
          style={{
            height: 40,
            borderWidth: 1,
            borderColor: color.pink,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{fontSize: 16, color: getTickColor('1year')}}>1y</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => getTickerData('5year')}
          style={{
            height: 40,
            borderWidth: 1,
            borderColor: color.pink,
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{fontSize: 16, color: getTickColor('5year')}}>5y</Text>
        </TouchableOpacity>
      </View>
    </>
  );
}

export default PriceChart;
