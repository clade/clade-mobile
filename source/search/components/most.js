import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Site from '../../helpers/site-helper';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {FlatList, Image, Text, TouchableHighlight, View} from 'react-native';
import LogoHelper from '../../helpers/logo-helper';
import globalStyles from '../../../assets/stylesheet/global-stylesheet';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import config from '../../../config';

let color = globalStyles.color;

function Most() {
  const [most, setMost] = useState([]);
  const [mostGains, setMostGains] = useState([]);
  const [mostActive, setMostActive] = useState([]);
  const [mostActiveButton, setMostActiveButton] = useState(true);
  const [mostGainsButton, setMostGainsButton] = useState(false);
  const navigation = useNavigation();
  const isFocused = useIsFocused();
  const [topMargin, setTopMargin] = useState(5);

  useEffect(() => {
    if (isFocused) {
      recentlyViewed().done();
    }
  }, [isFocused]);

  useEffect(() => {
    const fetchMost = async () => {
      const result = await axios(`${Site.getURL()}/most`);

      const {active, gains} = result.data;

      setMost(active.slice(0, 5));
      setMostActive(active.slice(0, 5));
      setMostGains(gains.slice(0, 5));
    };

    fetchMost().done();
  }, []);

  const renderMost = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('CompanyDetailsScreen', {symbol: item.symbol})
        }
        style={{
          marginBottom: 5,
          flexDirection: 'row',
          height: 45,
          borderWidth: 1,
          borderColor: color.blue,
          borderBottomColor: color.pink,
        }}>
        <View style={{flex: 0.12}}>
          <Image
            style={{borderRadius: 5, height: 30, width: 30}}
            source={{
              uri: LogoHelper.get(item.company_data[0].url),
            }}
          />
        </View>
        <View style={{flex: 0.65}}>
          <Text style={{marginTop: 0, color: 'white', fontSize: 16}}>
            {item.company_data[0].name}
          </Text>
        </View>
        <View style={{flex: 0.2}}>
          <Text
            style={{
              textAlign: 'right',
              marginTop: 10,
              color:
                item.percent_change < 0 ? color.stock_red : color.stock_green,
              fontSize: 16,
            }}>
            {item.percent_change}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  function setMostButton(most_type) {
    if (most_type === 'active') {
      setMost(mostActive);
    } else {
      setMost(mostGains);
    }
    setMostActiveButton(!mostActiveButton);
    setMostGainsButton(!mostGainsButton);
  }

  const recentlyViewed = async () => {

      const recentlyViewedKey = await AsyncStorage.getItem(
        config.recently_viewed_key,
      );

      if (recentlyViewedKey !== null) {
        if (JSON.parse(recentlyViewedKey).length > 0) {
          setTopMargin(5);
        } else {
          setTopMargin(40);
        }
      } else {
        setTopMargin(35);
      }
  };

  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          marginTop: topMargin,
        }}>
        <TouchableHighlight
          onPress={() => setMostButton('active')}
          style={{
            borderWidth: 1,
            borderColor: color.pink,
            backgroundColor: mostActiveButton ? color.pink : color.blue,
            margin: 5,
            height: 40,
            justifyContent: 'center',
            flex: 0.5,
            borderRadius: 5,
          }}>
          <Text
            style={{
              justifyContent: 'center',
              textAlign: 'center',
              color: 'white',
              fontSize: 24,
            }}>
            Most Active
          </Text>
        </TouchableHighlight>
        <TouchableHighlight
          onPress={() => setMostButton('gains')}
          style={{
            borderWidth: 1,
            borderColor: color.pink,
            backgroundColor: mostGainsButton ? color.pink : color.blue,
            margin: 5,
            height: 40,
            justifyContent: 'center',
            flex: 0.5,
            borderRadius: 5,
          }}>
          <Text
            style={{
              justifyContent: 'center',
              textAlign: 'center',
              color: 'white',
              fontSize: 24,
            }}>
            Most Gains
          </Text>
        </TouchableHighlight>
      </View>

      <View style={{marginTop: 20, marginBottom: 20}}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          legacyImplementation={false}
          data={most}
          renderItem={item => renderMost(item)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </View>
  );
}

export default Most;
