import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import globalStyles from '../../../assets/stylesheet/global-stylesheet';
import LogoHelper from '../../helpers/logo-helper';
import axios from 'axios';
import Site from '../../helpers/site-helper';
import SimilarTicker from './similar-ticker';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Feather';
import IndustryHelper from '../../helpers/industry-helper';

let color = globalStyles.color;
const {width} = Dimensions.get('window');

function Similar(props) {
  const [similarItems, setSimilarItems] = useState([]);
  const navigation = useNavigation();

  useEffect(() => {
    fetchSimilarItems().done();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchSimilarItems = async () => {
    const result = await axios(
      `${Site.getURL()}/similar_companies?symbol=${props.symbol}`,
    );
    let similarItemsResult = result.data
      .sort(() => Math.random() - Math.random())
      .slice(0, 2);
    setSimilarItems(similarItemsResult);
  };

  const header = () => {
    if (similarItems.length > 0) {
      return (
        <Text style={{color: 'white', fontSize: 24, marginBottom: 5}}>
          Similar Companies
        </Text>
      );
    }
  };

  const renderOne = () => {
    if (similarItems.length > 0) {
      let similarOne = similarItems[0];
      return (
        <TouchableOpacity
          onPress={() =>
            navigation.push('CompanyDetailsScreen', {symbol: similarOne.symbol})
          }
          style={{
            borderRadius: 10,
            height: 120,
            width: width / 2 - 30,
            backgroundColor: color.dark_gray,
            marginRight: 20,
          }}>
          <View style={{padding: 8}}>
            <View style={{flexDirection: 'row'}}>
              <Text style={{flex: 2, fontSize: 16, color: 'white'}}>
                {similarOne.symbol}
              </Text>
              <Image
                style={{borderRadius: 5, height: 30, width: 30}}
                source={{
                  uri: LogoHelper.get(similarOne.url),
                }}
              />
            </View>
            <View>
              <Text
                style={{marginTop: 5, fontSize: 12, color: color.light_gray}}>
                {similarOne.industry}
              </Text>
              <SimilarTicker symbol={similarOne.symbol} />
            </View>
          </View>
        </TouchableOpacity>
      );
    }
  };

  const renderTwo = () => {
    if (similarItems.length > 1) {
      let similarTwo = similarItems[1];
      return (
        <TouchableOpacity
          onPress={() =>
            navigation.push('CompanyDetailsScreen', {symbol: similarTwo.symbol})
          }
          style={{
            borderRadius: 10,
            height: 120,
            width: width / 2 - 30,
            backgroundColor: color.dark_gray,
            marginRight: 20,
          }}>
          <View style={{padding: 8}}>
            <View style={{flexDirection: 'row'}}>
              <Text style={{flex: 2, fontSize: 16, color: 'white'}}>
                {similarTwo.symbol}
              </Text>
              <Image
                style={{borderRadius: 5, height: 30, width: 30}}
                source={{
                  uri: LogoHelper.get(similarTwo.url),
                }}
              />
            </View>
            <View>
              <Text
                style={{marginTop: 5, fontSize: 12, color: color.light_gray}}>
                {similarTwo.industry}
              </Text>
              <SimilarTicker symbol={similarTwo.symbol} />
            </View>
          </View>
        </TouchableOpacity>
      );
    }
  };

  const industry = () => {
    if (similarItems.length > 0) {
      let industry_item = similarItems[0];
      let industry_data = IndustryHelper.get(industry_item.industry);

      return (
        <View>
          <Text
            style={{
              color: 'white',
              fontSize: 24,
              marginTop: 10,
              marginBottom: 10,
            }}>
            Industry
          </Text>

          <TouchableOpacity
            onPress={() =>
              navigation.navigate('CompanySearchByIndustry', {
                industry_item: industry_item,
                industry_data: industry_data,
              })
            }
            style={{
              borderRadius: 5,
              padding: 5,
              backgroundColor: industry_data.color,
              marginRight: 15,
              height: 100,
              width: 200,
            }}>
            <Icon name={industry_data.icon} size={28} color="white" />
            <Text
              style={{
                marginTop: 5,
                color: 'white',
                fontSize: 14,
                fontWeight: 'bold',
              }}>
              {industry_item.industry}
            </Text>
          </TouchableOpacity>
        </View>
      );
    }
  };

  return (
    <View style={style.container}>
      {header()}

      <View style={{flexDirection: 'row'}}>
        {renderOne()}
        {renderTwo()}
      </View>

      {industry()}
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.blue,
  },
});

export default Similar;
