import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, StyleSheet, Linking, Image} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import axios from 'axios';
import Site from '../helpers/site-helper';
import globalStyles from '../../assets/stylesheet/global-stylesheet';
import {TouchableOpacity} from 'react-native-gesture-handler';
import shortid from 'shortid';
import NewsHelper from '../helpers/news-helper';

let color = globalStyles.color;

function CompanyNewsScreen() {
  const navigation = useNavigation();
  const route = useRoute();
  const [news, setNews] = useState([]);
  const {symbol} = route.params;

  useEffect(() => {
    navigation.setOptions({
      title: symbol,
    });

    const fetchCompanyNews = async () => {
      const result = await axios(
        `${Site.getURL()}/news?symbol=${symbol}&limit=50`,
      );
      setNews(result.data);
    };

    fetchCompanyNews().done();
  });

  const renderNews = ({item}) => {
    return (
      <TouchableOpacity
        onPress={() => {
          Linking.openURL(item.article.url);
        }}>
        <View
          style={{
            marginBottom: 20,
            flex: 1,
            alignSelf: 'stretch',
            flexDirection: 'row',
          }}>
          <View style={{flex: 0.5, alignItems: 'flex-start'}}>
            <Image
              style={{marginTop: 10, height: 120, width: 120}}
              source={{
                uri: item.article.url_image,
              }}
            />
          </View>
          <View style={{marginTop: 10, flex: 1, alignItems: 'flex-start'}}>
            <Text style={{fontSize: 17, color: 'white', fontWeight: 'bold'}}>
              {item.title}
            </Text>

            <View style={{flexDirection: 'row'}}>
              <View style={{flex: 0.5, alignItems: 'flex-start'}}>
                <Text style={{marginTop: 14, color: 'white', fontSize: 18}}>
                  {NewsHelper.getDate(item.published_at.$date)}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        showsHorizontalScrollIndicator={false}
        legacyImplementation={false}
        data={news}
        renderItem={item => renderNews(item)}
        keyExtractor={() => shortid.generate()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#1a1e34',
    paddingTop: 10,
  },
});

export default CompanyNewsScreen;
