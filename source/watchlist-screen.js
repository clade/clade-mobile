import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, StyleSheet, Image, Alert} from 'react-native';
import config from '../config';
import axios from 'axios';
import Site from './helpers/site-helper';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Feather';
import LogoHelper from './helpers/logo-helper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import OS from './helpers/os';

function WatchListScreen() {
  const navigation = useNavigation();
  const [companyData, setCompanyData] = useState([]);
  const [deleteConfirm, setDeleteConfirm] = useState(true);
  const isFocused = useIsFocused();

  useEffect(() => {
    if (isFocused) {
      getWishList().done();
      settings().done();
    }
  }, [isFocused]);

  useEffect(() => {
    getWishList().done();
    settings().done();
  }, []);

  const settings = async () => {
    const settings_key = await AsyncStorage.getItem(config.settings_key);
    let settings = JSON.parse(settings_key);
    const {wish_list_delete_confirmation} = settings;
    setDeleteConfirm(wish_list_delete_confirmation);
  };

  const getWishList = async () => {
    const wishlist_key = await AsyncStorage.getItem(config.wish_list_key);
    let wishlist = JSON.parse(wishlist_key);

    const result = await axios(
      `${Site.getURL()}/companies_wish_list?symbols=${wishlist}`,
    );

    const {companies, ticker} = result.data;
    //todo: This should be handled by the API
    let merged_by_symbols = companies.map(x =>
      Object.assign(
        x,
        ticker.find(y => y.symbol === x.symbol),
      ),
    );

    setCompanyData(merged_by_symbols);
  };

  async function delFromWishList(symbol) {
    if (deleteConfirm) {
      Alert.alert(
        `${symbol}`,
        `Do you want to remove ${symbol} from you wishlist`,
        [
          {
            text: `Yes remove ${symbol}`,
            onPress: () => proceedToDelete(symbol).done(),
          },
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
        ],
        {cancelable: true},
      );
    } else {
      proceedToDelete(symbol).done();
    }
  }

  const proceedToDelete = async symbol => {
    const wishlist_key = await AsyncStorage.getItem(config.wish_list_key);
    if (wishlist_key !== null) {
      let wishlist = JSON.parse(wishlist_key);
      const index = wishlist.indexOf(symbol);
      if (index > -1) {
        wishlist.splice(index, 1);
        await AsyncStorage.setItem(
          config.wish_list_key,
          JSON.stringify(wishlist),
        );

        getWishList().done();
      }
    }
  };

  const calculateGainLoss = item => {
    let open = parseFloat(item.values.open);
    let close = parseFloat(item.values.close);
    return (open - close).toFixed(2);
  };

  const renderItem = ({item}) => (
    <View
      key={item.symbol}
      style={{flexDirection: 'row', marginTop: 10, marginBottom: 10}}>
      <View style={{flex: 0.9, height: 50}}>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('CompanyDetailsScreen', {symbol: item.symbol})
          }
          style={{flexDirection: 'row'}}>
          <View
            style={{
              marginRight: 5,
              justifyContent: 'center',
              alignItems: 'center',
              flex: 0.15,
            }}>
            <Image
              style={{borderRadius: 5, height: 35, width: 35}}
              source={{
                uri: LogoHelper.get(item.url),
              }}
            />
          </View>

          <View style={{flex: 0.7}}>
            <Text style={{color: 'white', fontSize: 17, marginBottom: 1}}>
              {item.name}
            </Text>
            <Text style={{color: 'gray'}}>{item.symbol}</Text>
          </View>

          <View style={{flex: 0.2, alignItems: 'flex-end', marginTop: 1}}>
            <Text style={{color: 'white'}}>
              {parseFloat(item.values.close).toFixed(2)}
            </Text>
            <Text
              style={{color: calculateGainLoss(item) > 0 ? 'green' : 'red'}}>
              {calculateGainLoss(item)}
            </Text>
          </View>
        </TouchableOpacity>
      </View>

      <View style={{flex: 0.1, alignItems: 'flex-end', marginTop: 5}}>
        <TouchableOpacity onPress={() => delFromWishList(item.symbol)}>
          <Icon name="trash" size={22} color="white" />
        </TouchableOpacity>
      </View>
    </View>
  );

  return (
    <View style={style.container}>
      <View
        style={{
          marginBottom: 20,
          paddingBottom: 5,
          borderBottomWidth: 1,
          borderBottomColor: 'pink',
        }}>
        <Text style={{color: 'white', fontSize: 24}}>Watchlist</Text>
      </View>
      <FlatList
        data={companyData}
        renderItem={renderItem}
        keyExtractor={item => item.symbol}
      />
    </View>
  );
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1a1e34',
    padding: 15,
  },
});

export default WatchListScreen;
