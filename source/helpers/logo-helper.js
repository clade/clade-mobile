class LogoHelper {
  static get(company_web) {
    return `https://logo.clearbit.com/${company_web}`;
  }
}

export default LogoHelper;
