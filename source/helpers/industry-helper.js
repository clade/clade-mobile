import industriesStyle from '../../assets/company/industries-style';

class IndustryHelper {
  static get(company) {
    let industryName = industriesStyle.find(
      industry => industry.industry === company,
    );
    if (industryName) {
      return industryName;
    } else {
      return {
        icon: 'dollar-sign',
        color: '#138049',
      };
    }
  }
}

export default IndustryHelper;
