import moment from 'moment';

class NewsHelper {
  static removePublisher(title, source_name) {
    return title.replace(`- ${source_name}`, '');
  }

  static getDate(date) {
    try {
      let dt = new Date(date);
      return moment(dt).fromNow();
    } catch (e) {
      return '';
    }
  }

  static getSourceImage(source) {
    return `https://logo.clearbit.com/${source}.com`;
  }
}

export default NewsHelper;
