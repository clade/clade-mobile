import site from '../../site-config';
// import site from '../../test-site-config';

class Site {
  static getURL() {
    return site.url;
  }
}

export default Site;
