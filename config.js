const config = {
  wish_list_key: '@WatchList:key',
  recently_viewed_key: '@RecentlyViewed:key',
  portfolio_key: '@Portfolio:key',
  settings_key: '@Settings:key',
  on_boarding_key: '@On_boarding:key',
  push: {
    pw_appid: '9C3CF-A0FE5',
    project_number: '130310965396',
  },
  auth_key: '@Auth:key',
  release_version: '@Release_version:key',
};

export default config;
